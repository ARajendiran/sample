package week2.day1;

public abstract class Radio {

	public void playStation(){
		
		System.out.println("play station");
	}
	
	public abstract void changeBattery();
	
	public static void main(String[] args) {
		
		//Radio rd = new Radio();
		//can not create object for abstract class
	}
}



