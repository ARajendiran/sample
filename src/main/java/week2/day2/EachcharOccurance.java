package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class EachcharOccurance {

	public static void main(String[] args) {

		String str = "Amazonindiaprivatelimited";
		char[] ch = str.toCharArray();

		Map <Character, Integer> map = new HashMap<>();
		int value = 0;
		for (char c : ch) {

			if(map.containsKey(c)){

				value = map.get(c);
				map.put(c, value+1);


			}else {

				map.put(c, 1);

			}

		}

		System.out.println(map);

	}

}
