package week2.day2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class FindmaxUsingmap {

	public static void main(String[] args) {

		Map <String, Integer> mobile = new HashMap<>();
		mobile.put("apple", 60000);
		mobile.put("oppo", 6000);
		mobile.put("samsung", 14000);
		mobile.put("vivo", 8000);
		mobile.put("redmi", 7500);

		//mobile.values();

		List <Integer> ls = new ArrayList<>();
		int max = 0;
		for (Integer mo : mobile.values()) {

			if (mo > max){

				max = mo;
			}

		}
		
		System.out.println(max);
		
		for (Entry <String, Integer> eachMobile : mobile.entrySet()){
			
			if(max ==eachMobile.getValue()){
				
				System.out.println(eachMobile.getKey()+ "--->" +eachMobile.getValue());
			}
		}
	}

}
