package week1.day2;

import java.util.Scanner;

public class DynamicArray {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int salaries []= new int[size];
		for (int i = 0; i<salaries.length; i++) {

			salaries[i] = sc.nextInt();
		}

		for (int salary: salaries) {

			System.out.println(salary);
		}
		sc.close();
	}



}
