package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {

	@Test
	public void report() throws IOException {

		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);

		ExtentTest logger = extent.createTest("TC002_CreateLead","create a new lead");
		logger.assignAuthor("abirami");
		logger.assignCategory("smoke");
		logger.log(Status.PASS, "sysout", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		logger.log(Status.PASS, "sysout1", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		logger.log(Status.PASS, "sysout2", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.pn").build());

		extent.flush();
	}
}

