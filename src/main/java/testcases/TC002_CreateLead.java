package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest (groups = {"any"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "gopi";
		category = "smoke";

	}
	@Test
	public void createLead() {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);*/
		click(locateElement("LinkText", "CRM/SFA"));
		click(locateElement("LinkText", "Create Lead"));
		type(locateElement("id","createLeadForm_companyName"), "abc limited");
		type(locateElement("id","createLeadForm_firstName"), "abhirami");
		type(locateElement("id","createLeadForm_lastName"), "rajendran");
		selectDropDownUsingText(locateElement("id","createLeadForm_industryEnumId"), "Press");
		//type(locateElement("xpath", ""))
		click(locateElement("name", "submitButton"));


	}

}

