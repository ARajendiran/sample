package home.exercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class Print2ndMaxPrice {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();

		driver.findElementByXPath("//input[@title='Search for products, brands and more']").sendKeys("iPhone X",
				Keys.ENTER);

		List<WebElement> priceElement = driver.findElementsByXPath("//div[@class='_1vC4OE']");

		List<String> price = new ArrayList<>();
		for (WebElement eachElement : priceElement) {
			price.add(eachElement.getText());
		}
		System.out.println(price);

		List<String> newPrice = new ArrayList<>();
		for (String eachPrice : price) {
			newPrice.add(eachPrice.replaceAll("[^0-9]", ""));
		}
		Set<String> uniquePrice = new TreeSet<>();
		uniquePrice.addAll(newPrice);

		System.out.println(uniquePrice);

		Integer firstMax = 0;
		for (String eachStr : uniquePrice) {
//		System.out.println(Integer.parseInt(eachStr));
			if (Integer.parseInt(eachStr) > firstMax) {
				firstMax = Integer.parseInt(eachStr);
			}
		}
		Integer secondMax = 0;

		for (String eachStr : uniquePrice) {
			if (Integer.parseInt(eachStr) > secondMax && Integer.parseInt(eachStr) != firstMax) {
				secondMax = Integer.parseInt(eachStr);
			}

		}

		String secondMaxStr = secondMax.toString();

		System.out.println(secondMaxStr);

		List<WebElement> mobName = driver.findElementsByXPath("//div[@class='_1vC4OE']/preceding::a[1]");

		Map<WebElement, String> priceName = new LinkedHashMap<>();

		for (int i = 0; i < mobName.size() && i < newPrice.size(); i++) {
			priceName.put(mobName.get(i), newPrice.get(i));
		}

		for (Entry<WebElement, String> eachPrice : priceName.entrySet()) {
			if (eachPrice.getValue().contains(secondMaxStr)) {
				System.out.println(eachPrice.getKey().getText());
				eachPrice.getKey().click();
			}
		}
		

	}

}
