package home.exercises;

import java.util.Scanner;

public class FloydsTriangle {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);

		System.out.println("Enter the number of rows: ");
		int rows=scan.nextInt();

		int num=1;

		for(int i=1; i<=rows; i++) {
			for(int j=1; j<=i; j++) {
				System.out.print(num+" ");
				num++;
			}
			System.out.print("\n");
		}

	}

}
