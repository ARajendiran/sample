package home.exercises;	

import java.text.DecimalFormat;
import java.util.Scanner;

public class FindPercentage {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);

		System.out.println("Enter the String");
		String str=scan.nextLine();

		int length=str.length();
		System.out.println(length);

		String upperStr=str.replaceAll("[^A-Z]", "");
		System.out.println(upperStr);
		int upperLen=upperStr.length();

		String lowerStr=str.replaceAll("[^a-z]", "");
		int lowerLen=lowerStr.length();

		String numStr=str.replaceAll("[^0-9]", "");
		int numLen=numStr.length();

		String splStr=str.replaceAll("[^\\W]", "");
		int splLen=splStr.length();

		double upperPercentage=((double)upperLen/(double)length)*(double)100;
		double lowerPercentage= ((double)lowerLen/(double)length)*(double)100;
		double numPercentage= (double)numLen/(double)length*(double)100;
		double splPercentage= (double)splLen/(double)length*(double)100;

		DecimalFormat df= new DecimalFormat("0.00");

		System.out.println("Number of uppercase letters is "+ upperLen+ ". "+ "So percentage is "+ df.format(upperPercentage)+"%");
		System.out.println("Number of lowercase letters is "+ lowerLen+ ". "+ "So percentage is "+ df.format(lowerPercentage)+"%");
		System.out.println("Number of digits is "+ numLen+ ". "+ "So percentage is "+ df.format(numPercentage)+"%");
		System.out.println("Number of other characters is "+ splLen+ ". "+ "So percentage is "+ df.format(splPercentage)+"%");

	}

}
