package home.exercises;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SecondLargesetWithoutDigit {

		public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Enter the number: ");
		int number=scan.nextInt();
		
		System.out.println("Enter the digit to compare: ");
		int digit = scan.nextInt();
		
		List<Integer> eachDigit=new ArrayList<>();
		
		int max=0;
		boolean flag=true;
		
		for(int i=1;i<number; i++) {
		int quot=i;
		while(quot!=0) {
			eachDigit.add(quot%10);
			quot=quot/10;
		}
		for (Integer digitList : eachDigit) {
			if(digitList==digit) {
				flag=false;
				break;
			}
		}
		if(flag==true && i>max) {
			max=i;
			
			}
		eachDigit.clear();	
		flag=true;
			
		}
		
		System.out.println("The largest number next to the given number " +number + " & which doesn't contain "+digit+ " is: "+max);
			

		
	}

}
