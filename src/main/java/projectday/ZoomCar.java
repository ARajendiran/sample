package projectday;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;


public class ZoomCar extends ProjectMethods{
	@BeforeTest
	public void setData(){

		testCaseName = "TC002_CreateLead";
		testDesc = "created lead opened";
		author = "Abhirami";
		category = "Exploratory";

	}
	@Test
	public void go(){

		click(locateElement("class", "search"));
		click(locateElement("xpath", "//div[@class='component-popular-locations']/div[2]"));
		//type(locateElement("class", "search"), "Velachery, Chennai, Tamil Nadu, India");
		click(locateElement("class", "proceed"));

		// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);

		click(locateElement("xpath", "//div[contains(text(),'"+tomorrow+"')]"));
		click(locateElement("class", "proceed"));
		click(locateElement("class", "proceed"));

		List <Integer> ls = new ArrayList<>();

		List<WebElement> allPrice = driver.findElementsByXPath("//div[@class = 'price']");
		for (WebElement webElement : allPrice) {

			String price = webElement.getText();
			ls.add(Integer.parseInt(price.replaceAll("[0-9]", " ")));		 		 	 

		}

		Collections.sort(ls);

		Integer maxPrice = ls.get(ls.size()-1);
		System.out.println("Maximum price for hire a car is "+maxPrice);


		String carBrand = locateElement("xpath", "//div[contains(text(),'"+maxPrice+"')]/preceding::h3[1]").getText();
		System.out.println("Brand with maximum cost is "+carBrand);
		//div[contains(text(),'14')]
	}




}
