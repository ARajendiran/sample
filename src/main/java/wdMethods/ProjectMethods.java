package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class ProjectMethods extends SeMethods {

	@BeforeSuite (groups = {"any"})
	public void beforeSuite() {
		startResult();
	}
	@BeforeMethod 
	public void login() {
		beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);


		/*@Parameters({"url", "username", "password"})
	@BeforeMethod 
	public void login(String url, String username, String password) {
		beforeMethod();
		startApp("chrome", url);
	    WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrm = locateElement("LinkText", "CRM/SFA");
		click(eleCrm);
		WebElement eleCreateLead = locateElement("LinkText", "Create Lead");
		click(eleCreateLead);
		 */	

	}

	@AfterMethod (groups = {"any"})
	public void closeApp() {
		closeBrowser();
	}

	@AfterSuite (groups = {"any"})
	public void endTestCase() {
		endResult();
	}

}
