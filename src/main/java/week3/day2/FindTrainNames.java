package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindTrainNames {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in/");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("BNC",Keys.TAB);
		Thread.sleep(3000);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");

		//find number of rows

		List<WebElement> allRows = table.findElements(By.tagName("tr"));
		System.out.println(allRows.size());
		for (int i =0; i< allRows.size(); i++) {
			List<WebElement> allColumn = allRows.get(i).findElements(By.tagName("td"));
			String text = allColumn.get(1).getText();

			/*		}
		WebElement secRow = allRows.get(1);


		//number of column in second row

		List<WebElement> allcolumn = secRow.findElements(By.tagName("td"));
		System.out.println(allcolumn.size());

		//train name in second row
			 */		
			//String text = allcolumn.get(1).getText();
			System.out.println(text);
		}

	}}
