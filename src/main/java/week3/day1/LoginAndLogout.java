package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginAndLogout {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("ABC limited");
		driver.findElementById("createLeadForm_firstName").sendKeys("Abhirami");
		driver.findElementById("createLeadForm_lastName").sendKeys("Rajendiran");

		// AND THE BELOW FOR THE DROPDOWN

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");

		WebElement source1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(source1);
		dd1.selectByValue("CATRQ_AUTOMOBILE");

		WebElement source2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 = new Select(source2);
		List<WebElement> allOptions = dd2.getOptions();
		for (WebElement eachOption : allOptions) {
			
			System.out.println(eachOption.getText());
			
			String text = eachOption.getText();

			if (text.startsWith("M")){

				System.out.println(text);

			}
		}

		driver.findElementByName("submitButton").click();
		driver.close();

	}

}
