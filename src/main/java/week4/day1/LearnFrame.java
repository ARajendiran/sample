package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text() = 'Try it']").click();
		Alert alertRef = driver.switchTo().alert();
		alertRef.getText();
		alertRef.sendKeys("Abirami Rajendiran");
		alertRef.accept();
		
		String name = driver.findElementByXPath("//p[@id='demo']").getText();
		if(name.contains("Abirami Rajendiran")) {
			
			System.out.println("Isdisplayed");
		}
		
		driver.switchTo().defaultContent();
		driver.findElementByXPath("//a[@title = 'Change Orientation']").click();
	}

}
