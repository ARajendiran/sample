package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class SwitchWindows {

	public static void main(String[] args) throws IOException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allWindow = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allWindow);
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		
		//taking snapshot
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File ("./snaps/img1.jpeg");
		FileUtils.copyFile(src, obj);
		
		//to close first window
		
		driver.switchTo().window(lst.get(0));
		driver.close();
		
		//driver.close();
	}

}
