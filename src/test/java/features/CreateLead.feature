Feature: Create Leads 
Background: 
	Given Launch the browser 
	And Enter the URL <url>
	

Scenario Outline: Create Lead 
	And Enter the username as <username>
	And Enter the password as <password>
	And Click on the Login Button 
	And click on CRMSFA 
	And Click on Create Leads 
	And Enter Company Name <companyName>
	And Enter First name <FirstName>
	And Enter Last Name <LastName>
	When Click Submit Button 
	Then Verify Create Lead is successful 
	Then Close the Browser 
	
	Examples: 
		|url|username|password|companyName|FirstName|LastName|
		|http://leaftaps.com/opentaps/control/main|DemoSalesManager|crmsfa|RGBS|Rajeev|Ranjan|
		|http://leaftaps.com/opentaps/control/main|DemoCSR|crmsfa|RGBS|Rajeev|Ranjan|