package stepImplementations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {

	public static ChromeDriver driver;

	@Given("Launch the browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@And ("Enter the URL (.*)")
	public void enterTheURL(String url) {
		driver.get(url);
	}

	@And ("Enter the username as (.*)")	
	public void enterTheUsername(String username) {		
		driver.findElementById("username").sendKeys(username);
	}
	@And ("Enter the password as (.*)")	
	public void enterThePassword(String password) {		
		driver.findElementById("password").sendKeys(password);
	}

	@And ("Click on the Login Button")
	public void clickLoginButton() {		
		driver.findElementByClassName("decorativeSubmit").click();		
	}

	@Given("click on CRMSFA")
	public void clickOnCRMSFA() {
		driver.findElementByPartialLinkText("CRM/SFA").click();
	}

	@Given("Click on Create Leads")
	public void clickOnCreateLeads() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter Company Name (.*)")
	public void enterCompanyName(String companyName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);

	}

	@Given("Enter First name (.*)")
	public void enterFirstName(String FirstName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(FirstName);
	}


	@Given("Enter Last Name (.*)")
	public void enterLastName(String LastName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(LastName);
	}

	@When("Click Submit Button")
	public void clickSubmitButton() {
		driver.findElementByClassName("smallSubmit").click();

	}

	@Then("Verify Create Lead is successful")
	public void verifyCreateLeadIsSuccessful() {
		String pageTitle = driver.getTitle();
		if (pageTitle.equalsIgnoreCase("View Lead")) {
			System.out.println("Create Lead is successful");
		}
	}

	@Then ("Close the Browser")
	public void CloseBroser() {
		driver.close();
	}

}